﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class PersonController : Controller
    {
        // GET: Person
        public ActionResult Index()
        {
            Person pp = new Person()
            {
                First_Name = "srinivas",
                Last_Name = "lakkineni",
                Age = 33,
                Gender = "Male"
            };

            return View(pp);
        }

       
    }
}