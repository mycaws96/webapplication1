﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Person

    {
        public String First_Name { get; set; }
        public String Last_Name { get; set; }
        public int Age { get; set; }
        public String Gender { get; set; }

    }
}